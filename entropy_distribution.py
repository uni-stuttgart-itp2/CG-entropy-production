import numpy as np
import tqdm
from multiprocessing import Pool
import pickle as pick
from systems.calmodulin import calmodulin
from protocols.force_ramp import force_ramp
from aux import no_progressbar
import utils


np.set_printoptions(edgeitems=30, linewidth=100000, formatter=dict(float=lambda x: "%.3g" % x))


def multiprocessing_P_s(args):
    system, protocol_f, params = args
    result = utils.calc_s_of_tau_t(system, protocol_f, params)
    return result


# control parameter
save = False
multiprocessing = True
progress_bar = True

# simulation effort
N_t_WTD = int(1e2)
N_t_stoch = int(2e5)
N_t_hist = int(2e1)
sample_base = int(1e6)
N_threads = 15

# parameter
T = 1.5
f0 = 7
f1 = 12
slope = (f1-f0) / T
dS = 1e-1

params = []
if multiprocessing:
    sample_base = int(sample_base/N_threads)
for i in range(N_threads):
    if ((not multiprocessing) or i == 0) and progress_bar:
        params.append({"T": T,
                       "f0": f0,
                       "slope": slope,
                       "dS": dS,
                       "sample_base": sample_base,
                       "N_t_WTD": N_t_WTD,
                       "N_t_stoch": N_t_stoch,
                       "num": i,
                       "progress_bar_function": tqdm.tqdm,
                       "mode": "v"})
    else:
        params.append({"T": T,
                       "f0": f0,
                       "slope": slope,
                       "dS": dS,
                       "sample_base": sample_base,
                       "N_t_WTD": N_t_WTD,
                       "N_t_stoch": N_t_stoch,
                       "num": i,
                       "progress_bar_function": no_progressbar,
                       "mode": "silent"})

# system
system = calmodulin

# protocol
protocol_f = force_ramp(T, f0, slope, reverse=False)
protocol_r = force_ramp(T, f0, slope, reverse=True)

# main
WTDs, rWTDs, Pis = utils.calc_WTDs(system, protocol_f, protocol_r, params[0])
N_event = utils.calc_normalizations(WTDs, Pis, params[0])
x_S, P_S = utils.calc_P_S(WTDs, rWTDs, Pis, N_event, params[0])
if multiprocessing is True:
    args = []
    for i in range(N_threads):
        args.append([system, protocol_f, params[i]])
    results_temp = []
    process = Pool(processes=N_threads)
    for result in process.imap(multiprocessing_P_s, args):
        results_temp.append(result)
    process.close()
    process.join()
    results = []
    for temp in results_temp:
        for res in temp:
            results.append(res)
else:
    results = utils.calc_s_of_tau_t(system, protocol_f, params[0])
s_of_t0_t1 = np.zeros((N_t_hist, N_t_hist))
p_of_t0_t1 = np.zeros((N_t_hist, N_t_hist))
dt_snip = T/N_t_hist
for res in results:
    pos_t0 = int(res[1] / dt_snip)
    pos_t1 = int(res[2] / dt_snip)
    p_of_t0_t1[pos_t0, pos_t1] += 1
    s_of_t0_t1[pos_t0, pos_t1] += res[0]
P_s = [0]
x_s = [0]
for S in results:
    pos = int((S[0] - x_s[0]) / dS)
    while pos < 0:
        P_s.insert(0, 0)
        x_s.insert(0, x_s[0] - dS)
        pos = int((S[0] - x_s[0]) / dS)
    while pos >= len(P_s):
        P_s.append(0)
        x_s.append(x_s[-1] + dS)
    P_s[pos] += 1
P_s = np.array(P_s, dtype=float)
P_s /= sum(P_s) * dS

# saving
results_data = {"P_S": P_S,
                "x_S": x_S,
                "P_s": P_s,
                "x_s": x_s,
                "p_of_t0_t1": p_of_t0_t1,
                "s_of_t0_t1": s_of_t0_t1,
                "t0": np.linspace(0, T, N_t_hist),
                "t1": np.linspace(0, T, N_t_hist)}
data = {
    "results": results_data,
    "params": {
        "T": T,
        "f0": f0,
        "f1": f1,
        "N_t (WTDs)": N_t_WTD,
        "N_t (stoch)": N_t_stoch,
        "dS (bin width)": dS,
        "sample_base": sample_base
        },
    "info": "protocol: force_ramp; system: calmodulin"
    }

if save:
    f = open("PATH_TO_SAVE", "wb")
    pick.dump(data, f)
    f.close()

import matplotlib.pyplot as plt
plt.figure(1)
plt.plot(x_S, P_S)
plt.plot(x_s, P_s)
plt.figure(2)
plt.imshow(p_of_t0_t1, cmap="viridis", extent=[0, 1.5, 0, 1.5], aspect="auto", origin="lower")
plt.figure(3)
plt.imshow(s_of_t0_t1, cmap="viridis", extent=[0, 1.5, 0, 1.5], aspect="auto", origin="lower")
plt.show()
