class protocol:
    def __init__(self, T, reverse=False):
        self.T = T
        self.reverse = reverse

    def tau(self, t):
        if t > self.T:
            t = self.T
        if self.reverse:
            return self.T - t
        else:
            return t
