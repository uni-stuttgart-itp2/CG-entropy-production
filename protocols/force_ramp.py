from protocols.base_protocol import protocol


class force_ramp(protocol):
    def __init__(self, T, f0, slope, reverse=False):
        super().__init__(T, reverse)
        self.slope = slope
        self.f0 = f0

    def f(self, t):
        return self.f0 + self.slope * self.tau(t)
