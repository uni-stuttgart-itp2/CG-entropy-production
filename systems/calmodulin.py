import numpy as np
from systems.base_system import TD_markov_network


class calmodulin(TD_markov_network):
    def __init__(self, protocol):
        super().__init__(protocol, 6)
        self.K0 = np.array([
            [0, 0.2, 0.16, 0, 0, 0],                                                               # F1234 : 0
            [3.8e8, 0, 0, 1.1e7, 0, 1e-5],                                                         # F12   : 1
            [2.4e9, 0, 0, 0, 0, 7.9e-5],                                                           # F34   : 2
            [0, 0.74, 0, 0, 0, 0],                                                                 # F123  : 3
            [0, 0, 0, 0, 0, 0.04],                                                                 # F23   : 4
            [0, 2.9e10, 1.3e11, 0, 4.9e7, 0]                                                       # U     : 5
            ])
        a = self.K0[0, 1] * self.K0[1, 5] * self.K0[5, 2] * self.K0[2, 0]
        b = self.K0[0, 2] * self.K0[2, 5] * self.K0[5, 1] * self.K0[1, 0]
        self.K0[5, 2] *= b / a

        self.K0_orig = self.K0.copy()
        self.Kappa = np.array([
            [0, 0.33, 0.43, 0, 0, 0],                                                              # F1234 : 0
            [-1.9, 0, 0, -1, 0, 1.2],                                                              # F12   : 1
            [-2.2, 0, 0, 0, 0, 1],                                                                 # F34   : 2
            [0, 0.47, 0, 0, 0, 0],                                                                 # F123  : 3
            [0, 0, 0, 0, 0, 0.89],                                                                 # F23   : 4
            [0, -2.5, -2.6, 0, -1.7, 0]                                                            # U     : 5
            ])
        c = self.Kappa[0, 1] + self.Kappa[1, 5] + self.Kappa[5, 2] + self.Kappa[2, 0]
        d = self.Kappa[0, 2] + self.Kappa[2, 5] + self.Kappa[5, 1] + self.Kappa[1, 0]
        self.Kappa[5, 2] += d - c

    def set_absorbing_states(self, absorbing_states):
        self.K0 = self.K0_orig.copy()
        for state in absorbing_states:
            self.K0[state, :] = 0

    def get_K(self, t):
        return self.K0 * np.exp(self.Kappa * self.protocol.f(t))

    def get_K_orig(self, t):
        return self.K0_orig * np.exp(self.Kappa * self.protocol.f(t))
