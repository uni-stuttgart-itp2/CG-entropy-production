import numpy as np


class TD_markov_network:
    def __init__(self, protocol, N):
        self.protocol = protocol
        self.N = N
        self.K = np.zeros((self.N, self.N))
        self.L = np.zeros((self.N, self.N))

    def get_initial_distribution_state_exit(self, s, t0):
        K = self.get_K_orig(t0)
        p0 = K[s, :]
        p0 /= sum(p0)
        return p0

    def get_initial_distribution_after_link(self, link, t0):
        p0 = np.zeros(self.N)
        p0[link[1]] = 1
        return p0

    def get_initial_distribution_steady_state(self, t0):
        L = self.get_L_orig(t0)
        val, vec = np.linalg.eig(L)
        ind = np.argsort(-val)
        val = val[ind]
        vec = vec[:, ind]
        p0 = np.real(vec[:, 0] / sum(vec[:, 0]))
        return p0

    def get_K(self, t):
        return self.K

    def get_K_orig(self, t):
        return self.K

    def get_L(self, t):
        K = self.get_K(t).copy()
        L = np.transpose(K)
        for i in range(self.N):
            L[i, i] = -sum(K[i, :])
        return L

    def get_L_orig(self, t):
        K_orig = self.get_K_orig(t).copy()
        L_orig = np.transpose(K_orig)
        for i in range(self.N):
            L_orig[i, i] = -sum(K_orig[i, :])
        return L_orig


class SS_markov_network:
    def __init__(self, N):
        self.N = N
        self.K = np.zeros((self.N, self.N))
        self.L = np.zeros((self.N, self.N))

    def get_initial_distribution_state_exit(self, s):
        K = self.get_K_orig()
        p0 = K[s, :].copy()
        p0 /= sum(p0)
        return p0

    def get_initial_distribution_after_link(self, link):
        p0 = np.zeros(self.N)
        p0[link[1]] = 1
        return p0

    def get_initial_distribution_steady_state(self):
        L = self.get_L_orig()
        val, vec = np.linalg.eig(L)
        ind = np.argsort(-val)
        val = val[ind]
        vec = vec[:, ind]
        p0 = vec[:, 0] / sum(vec[:, 0])
        return p0

    def get_K(self, t):
        return self.K

    def get_K_orig(self, t):
        return self.K

    def get_L(self):
        K = self.get_K().copy()
        L = np.transpose(K)
        for i in range(self.N):
            L[i, i] = -sum(K[i, :])
        return L

    def get_L_orig(self):
        K_orig = self.get_K_orig().copy()
        L_orig = np.transpose(K_orig)
        for i in range(self.N):
            L_orig[i, i] = -sum(K_orig[i, :])
        return L_orig
