import numpy as np
from simulation import TD_simulation_deterministic
from simulation import TD_simulation_stochastic
from aux import print_debug


np.set_printoptions(edgeitems=30, linewidth=100000, formatter=dict(float=lambda x: "%.3g" % x))


def calc_WTDs(system, protocol_f, protocol_r, params):
    # calculate CG path weights

    # parameter
    T = params["T"]
    N_t = params["N_t_WTD"]
    numeric_params = {"t_min": 0, "t_max": T, "N_t": N_t}
    t = np.linspace(0, T, N_t)
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    # initialize system-classes and simulation-classes
    system_default = system(protocol_f)
    sim_default = TD_simulation_deterministic(system_default, numeric_params)
    system_f = system(protocol_f)
    system_f.set_absorbing_states([0, 5])
    sim_f = TD_simulation_deterministic(system_f, numeric_params)
    system_r = system(protocol_r)
    system_r.set_absorbing_states([0, 5])
    sim_r = TD_simulation_deterministic(system_r, numeric_params)

    # solution for p(t)
    p0 = system_default.get_initial_distribution_steady_state(0)
    simulation_params = {"t_start": 0, "t_end": T, "p0": p0}
    sim_default.set_simulation_params(simulation_params)
    res_orig = sim_default.get_pt()
    
    # Psi(t0, t1):
    # t1, t0 in absolute forward time,
    # t0: time of the initial event,
    # t1: final time of the snippet,
    # t = t1 - t0: duration of the snippet
    WTDs = {}
    WTDs["F1234-U"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    WTDs["rU-rF1234"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    WTDs["F1234-rF1234"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    WTDs["rU-U"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))

    rWTDs = {}
    rWTDs["F1234-U"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    rWTDs["rU-rF1234"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    rWTDs["F1234-rF1234"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    rWTDs["rU-U"] = np.zeros((numeric_params["N_t"], numeric_params["N_t"]))
    
    # Pi(t0): t0 in absolute forward time
    Pis = {}
    Pis["F1234"] = np.zeros(numeric_params["N_t"])
    Pis["rF1234"] = np.zeros(numeric_params["N_t"])
    Pis["U"] = np.zeros(numeric_params["N_t"])
    Pis["rU"] = np.zeros(numeric_params["N_t"])

    print_debug("calculate forward PSIs", mode)
    for i in progress_bar_function(range(N_t)):
        # loop over all t0
        
        # p0: distribution after leaving F1234 (state 0) at t0
        p0 = system_f.get_initial_distribution_state_exit(0, t[i])
        
        # simulation starts at t0 with p0 until T
        sim_f.simulation_params = {"t_start": t[i], "t_end": T, "p0": p0}
        
        # get p(t) for the given initial conditions
        # res["p"][a,b]: p_a(t0+b*dt): probability for state a at absolute time t0+b*dt
        res_F1234 = sim_f.get_pt()

        # p0: distribution after leaving U (state 5) at t0
        p0 = system_f.get_initial_distribution_state_exit(5, t[i])

        # simulation starts at t0 with p0 until T
        sim_f.simulation_params = {"t_start": t[i], "t_end": T, "p0": p0}
        
        # get p(t) for the given initial conditions
        # res["p"][a,b]: p_a(t0+b*dt): probability for state a at absolute time t0+b*dt
        res_rU = sim_f.get_pt()
        
        for j in range(i, N_t):
            # loop over all t1 beginning at t0 (due to t1 > t0)
            
            # Psi_F1234_U(t0,t1) = sum_n (p_n(t1)*k_{n -> U}(t1)), U: state 5
            WTDs["F1234-U"][i, j] = sum(res_F1234["p"][:, j - i] * system_f.get_K_orig(t[j])[:, 5])

            # Psi_F1234_rF1234(t0,t1) = sum_n (p_n(t1)*k_{n -> F1234}(t1)), F1234: state 0
            WTDs["F1234-rF1234"][i, j] = sum(res_F1234["p"][:, j - i] * system_f.get_K_orig(t[j])[:, 0])

            # Psi_rU_rF1234(t0,t1) = sum_n (p_n(t1)*k_{n -> F1234}(t1)), F1234: state 0
            WTDs["rU-rF1234"][i, j] = sum(res_rU["p"][:, j - i] * system_f.get_K_orig(t[j])[:, 0])

            # Psi_rU_U(t0,t1) = sum_n (p_n(t1)*k_{n -> U}(t1)), U: state 5
            WTDs["rU-U"][i, j] = sum(res_rU["p"][:, j - i] * system_f.get_K_orig(t[j])[:, 5])
            
        # Pi_F1234: p_F1234(t0) * sum_n k_{F1234 -> n}(t0)
        Pis["F1234"][i] = res_orig["p"][0, i] * sum(system_f.get_K_orig(t[i])[0, :])

        # Pi_rF1234: sum_n (p_n(t0) * k_{n -> F1234}(t0))
        Pis["rF1234"][i] = sum(res_orig["p"][:, i] * system_f.get_K_orig(t[i])[:, 0])

        # Pi_U: sum_n (p_n(t0) * k_{n -> U}(t0)
        Pis["U"][i] = sum(res_orig["p"][:, i] * system_f.get_K_orig(t[i])[:, 5])

        # Pi_rU: p_U(t0) * sum_n k_{U -> n}(t0)
        Pis["rU"][i] = res_orig["p"][5, i] * sum(system_f.get_K_orig(t[i])[5, :])

    print_debug("calculate backward PSI", mode)
    for i in progress_bar_function(range(N_t)):
        # loop over all t0

        # p0: distribution after leaving U (state 5) at t0
        p0 = system_r.get_initial_distribution_state_exit(5, t[i])

        # simulation starts at t0 with p0 until T. The protocol runs backwards: rlambda(t0) -> rlambda(T) <-> lambda(T-t0) -> lambda(0)
        sim_r.simulation_params = {"t_start": t[i], "t_end": T, "p0": p0}

        # get p(t) for the given initial conditions
        # res["p"][a,b]: p_a(t0+b*dt): probability for state a at absolute time t0+b*dt. The protocol runs backwards.
        res_rU = sim_r.get_pt()

        # p0: distribution after leaving F1234 (state 0) at t0
        p0 = system_r.get_initial_distribution_state_exit(0, t[i])

        # simulation starts at t0 with p0 until T. The protocol runs backwards: rlambda(t0) -> rlambda(T) <-> lambda(T-t0) -> lambda(0)
        sim_r.simulation_params = {"t_start": t[i], "t_end": T, "p0": p0}

        # get p(t) for the given initial conditions
        # res["p"][a,b]: p_a(t0+b*dt): probability for state a at absolute time t0+b*dt. The protocol runs backwards.
        res_F1234 = sim_r.get_pt()
        
        for j in range(i, N_t):
            # loop over all t1 beginning at t0 (due to t1 > t0)

            # In the WTDs the arguments are in absolute FORWARD times. All other time arguments in this section are with respect to the
            # BACKWARDS protocol.

            # Psi_rU_rF1234(t0,t1) = sum_n (p_n(t1)*k_{n -> F1234}(t1)), F1234: state 0.
            rWTDs["rU-rF1234"][-(i + 1), -(j + 1)] = sum(res_rU["p"][:, j - i] * system_r.get_K_orig(t[j])[:, 0])

            # Psi_rU_U(t0,t1) = sum_n (p_n(t1)*k_{n -> U}(t1)), F1234: state 5.
            rWTDs["rU-U"][-(i + 1), -(j + 1)] = sum(res_rU["p"][:, j - i] * system_r.get_K_orig(t[j])[:, 5])

            # Psi_rU_rF1234(t0,t1) = sum_n (p_n(t1)*k_{n -> F1234}(t1)), F1234: state 0.
            rWTDs["F1234-rF1234"][-(i + 1), -(j + 1)] = sum(res_F1234["p"][:, j - i] * system_r.get_K_orig(t[j])[:, 0])

            # Psi_rU_U(t0,t1) = sum_n (p_n(t1)*k_{n -> U}(t1)), F1234: state 5.
            rWTDs["F1234-U"][-(i + 1), -(j + 1)] = sum(res_F1234["p"][:, j - i] * system_r.get_K_orig(t[j])[:, 5])

    return WTDs, rWTDs, Pis


def calc_normalizations(WTDs, Pis, params):
    # calculate relative probability of each kind of process

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    N_event = {"F1234-rF1234": 0,
               "F1234-U": 0,
               "rU-U": 0,
               "rU-rF1234": 0}

    print_debug("calculate normalization", mode)
    # normalizations
    for i in progress_bar_function(range(N_t)):
        # integral over t0 (0 -> T)
        
        for j in range(N_t - i):
            # integral over t (0 -> T-t0)
            
            N_event["F1234-U"] += dt**2 * Pis["F1234"][i] * WTDs["F1234-U"][i, i + j]
            N_event["F1234-rF1234"] += dt**2 * Pis["F1234"][i] * WTDs["F1234-rF1234"][i, i + j]
            N_event["rU-rF1234"] += dt**2 * Pis["rU"][i] * WTDs["rU-rF1234"][i, i + j]
            N_event["rU-U"] += dt**2 * Pis["rU"][i] * WTDs["rU-U"][i, i + j]

    return N_event


def calc_S(WTDs, rWTDs, Pis, N_event, params):
    # calculate CG entropy production

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]
    
    # calculate S
    DeltaS = {"F1234-rF1234": 0.0,
              "F1234-U": 0.0,
              "rU-U": 0.0,
              "rU-rF1234": 0.0}
    print_debug("calculate S", mode)
    for i in progress_bar_function(range(N_t)):
        # integral over t0 (0 -> T)
        
        for j in range(N_t - i):
            # integral over t (0 -> T-t0)

            S_snip_est_inc = dt**2 / N_event["F1234-U"] * Pis["F1234"][i] * WTDs["F1234-U"][i, i + j] \
                * np.log((Pis["F1234"][i] * WTDs["F1234-U"][i, i + j])/(Pis["rU"][i + j] * rWTDs["rU-rF1234"][i + j, i]))
            if (not np.isnan(S_snip_est_inc)) and (not np.isinf(S_snip_est_inc)):
                DeltaS["F1234-U"] += S_snip_est_inc
            S_snip_est_inc = dt**2 / N_event["F1234-rF1234"] * Pis["F1234"][i] * WTDs["F1234-rF1234"][i, i + j] \
                * np.log((Pis["F1234"][i] * WTDs["F1234-rF1234"][i, i + j])/(Pis["F1234"][i + j] * rWTDs["F1234-rF1234"][i + j, i]))
            if (not np.isnan(S_snip_est_inc)) and (not np.isinf(S_snip_est_inc)):
                DeltaS["F1234-rF1234"] += S_snip_est_inc
            S_snip_est_inc = dt**2 / N_event["rU-U"] * Pis["rU"][i] * WTDs["rU-U"][i, i + j] \
                * np.log((Pis["rU"][i] * WTDs["rU-U"][i, i + j])/(Pis["rU"][i + j] * rWTDs["rU-U"][i + j, i]))
            if (not np.isnan(S_snip_est_inc)) and (not np.isinf(S_snip_est_inc)):
                DeltaS["rU-U"] += S_snip_est_inc
            S_snip_est_inc = dt**2 / N_event["rU-rF1234"] * Pis["rU"][i] * WTDs["rU-rF1234"][i, i + j] \
                * np.log((Pis["rU"][i] * WTDs["rU-rF1234"][i, i + j])/(Pis["F1234"][i + j] * rWTDs["F1234-U"][i + j, i]))
            if (not np.isnan(S_snip_est_inc)) and (not np.isinf(S_snip_est_inc)):
                DeltaS["rU-rF1234"] += S_snip_est_inc
    return DeltaS


def calc_S_of_t(WTDs, rWTDs, Pis, N_event, params):
    # calculate CG entropy production given duration t

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    # calculate S(t)
    S_est_of_t = np.zeros(N_t)
    print_debug("calculate S(t)", mode)
    for j in progress_bar_function(range(N_t)):
        # loop over t (0 -> T)

        for i in range(N_t - j):
            # integral over tau (0 -> T-t)

            S_snip_est_inc = dt / N_event["F1234-U"] * Pis["F1234"][i] * WTDs["F1234-U"][i, i + j] * \
                np.log((Pis["F1234"][i] * WTDs["F1234-U"][i, i + j]) / (Pis["rU"][i + j] * rWTDs["rU-rF1234"][i + j, i]))

            if np.isnan(S_snip_est_inc) == False:
                S_est_of_t[j] += S_snip_est_inc
    return S_est_of_t


def calc_S_of_tau(WTDs, rWTDs, Pis, N_event, params):
    # calculate CG entropy production given initial time tau

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    # calculate S(tau)
    S_est_of_tau = np.zeros(N_t)
    print_debug("calculate S(tau)", mode)
    for i in progress_bar_function(range(N_t)):
        # loop over tau (0 -> T)

        for j in range(N_t - i):
            # integral over t (0 -> T-tau)

            S_snip_est_inc = dt / N_event["F1234-U"] * Pis["F1234"][i] * WTDs["F1234-U"][i, i + j] * \
                np.log((Pis["F1234"][i] * WTDs["F1234-U"][i, i + j]) / (Pis["rU"][i + j] * rWTDs["rU-rF1234"][i + j, i]))
            if np.isnan(S_snip_est_inc) == False:
                S_est_of_tau[i] += S_snip_est_inc
                
    return S_est_of_tau


def calc_P_S(WTDs, rWTDs, Pis, N_event, params):
    # calculate distribution of CG entropy production

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]
    dS = params["dS"]

    print_debug("calculate P_S", mode)

    # calculate P_S
    P_S = [0]
    x_S = [0]
    for i in progress_bar_function(range(N_t)):
        # integral over t0 (0 -> T)

        for j in range(N_t - i):
            # integral over t (0 -> T-t0)

            P_snip = Pis["F1234"][i] * WTDs["F1234-U"][i, i + j] / N_event["F1234-U"] * dt**2
            S_snip = np.log((Pis["F1234"][i] * WTDs["F1234-U"][i, i + j]) / (Pis["rU"][i + j] * rWTDs["rU-rF1234"][i + j, i]))
            pos = int((S_snip - x_S[0]) / dS)
            while pos < 0:
                P_S.insert(0, 0)
                x_S.insert(0, x_S[0] - dS)
                pos = int((S_snip - x_S[0]) / dS)
            while pos >= len(P_S):
                P_S.append(0)
                x_S.append(x_S[-1] + dS)
            P_S[pos] += P_snip / dS

    return x_S, P_S


def calc_s(system, protocol_f, params):
    # calculate microscopic entropy production

    # parameter
    T = params["T"]
    N_t_stoch = params["N_t_stoch"]
    sample_base = params["sample_base"]
    numeric_params_stochastic = {"t_min": 0, "t_max": T, "N_t": N_t_stoch}
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    print_debug("calculate s_est", mode)

    # initialize system-classes and simulation-classes
    system_stochastic = system(protocol_f)
    sim_stochastic = TD_simulation_deterministic(system_stochastic, numeric_params_stochastic)
    system_f = system(protocol_f)
    sim_stoch_f = TD_simulation_stochastic(system_f)

    # solve System
    p0 = system_stochastic.get_initial_distribution_steady_state(0)
    simulation_params = {"t_start": 0, "t_end": T, "p0": p0}
    sim_stochastic.set_simulation_params(simulation_params)
    res_orig_stochastic = sim_stochastic.get_pt()

    # calculate real entropy production between F1234 and U
    samples = int(sample_base / T)
    s_snip_raw, N_raw = sim_stoch_f.s_AB_all_combinations(res_orig_stochastic["p"], res_orig_stochastic["t"], 0, 5,
                                                          samplesize=samples, progress_bar_function=progress_bar_function)
    s_snip = {}
    for key in s_snip_raw.keys():
        s_snip[key] = np.mean(s_snip_raw[key])
    return s_snip, N_raw


def calc_s_of_t(system, protocol_f, params):
    # calculate microscopic entropy production as function of the duration t

    # parameter
    T = params["T"]
    N_t_stoch = params["N_t_stoch"]
    sample_base = params["sample_base"]
    numeric_params_stochastic = {"t_min": 0, "t_max": T, "N_t": N_t_stoch}
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]
    samples = int(sample_base / T)

    print_debug("calculate s(t)", mode)

    # initialize system-classes and simulation-classes
    system_stochastic = system(protocol_f)
    sim_stochastic = TD_simulation_deterministic(system_stochastic, numeric_params_stochastic)

    # solve System
    p0 = system_stochastic.get_initial_distribution_steady_state(0)
    simulation_params = {"t_start": 0, "t_end": T, "p0": p0}
    sim_stochastic.set_simulation_params(simulation_params)
    res_orig_stochastic = sim_stochastic.get_pt()

    # stochastic simulation
    system_f = system(protocol_f)
    sim_stoch_f = TD_simulation_stochastic(system_f)
    res_stochastic = sim_stoch_f.s_A_to_B_t(
        res_orig_stochastic["p"], res_orig_stochastic["t"], 0, 5, samplesize=samples, progress_bar_function=progress_bar_function
        )

    return res_stochastic


def calc_s_of_tau(system, protocol_f, params):
    # calculate microscopic entropy production as function of the initial time tau

    # parameter
    T = params["T"]
    N_t_stoch = params["N_t_stoch"]
    sample_base = params["sample_base"]
    numeric_params_stochastic = {"t_min": 0, "t_max": T, "N_t": N_t_stoch}
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]
    samples = int(sample_base / T)

    print_debug("calculate s(tau)", mode)

    # initialize system-classes and simulation-classes
    system_stochastic = system(protocol_f)
    sim_stochastic = TD_simulation_deterministic(system_stochastic, numeric_params_stochastic)

    # solve System
    p0 = system_stochastic.get_initial_distribution_steady_state(0)
    simulation_params = {"t_start": 0, "t_end": T, "p0": p0}
    sim_stochastic.set_simulation_params(simulation_params)
    res_orig_stochastic = sim_stochastic.get_pt()

    # stochastic simulation
    system_f = system(protocol_f)
    sim_stoch_f = TD_simulation_stochastic(system_f)
    res_stochastic = sim_stoch_f.s_A_to_B_tau_t(
        res_orig_stochastic["p"], res_orig_stochastic["t"], 0, 5, samplesize=samples, progress_bar_function=progress_bar_function
        )

    return res_stochastic


def calc_s_of_tau_t(system, protocol_f, params):
    # calculate microscopic entropy production as function of initial the time tau and the duration t

    # parameter
    T = params["T"]
    N_t_stoch = params["N_t_stoch"]
    sample_base = params["sample_base"]
    numeric_params_stochastic = {"t_min": 0, "t_max": T, "N_t": N_t_stoch}
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]
    samples = int(sample_base / T)

    print_debug("calculate s(t)", mode)

    # initialize system-classes and simulation-classes
    system_stochastic = system(protocol_f)
    sim_stochastic = TD_simulation_deterministic(system_stochastic, numeric_params_stochastic)

    # solve System
    p0 = system_stochastic.get_initial_distribution_steady_state(0)
    simulation_params = {"t_start": 0, "t_end": T, "p0": p0}
    sim_stochastic.set_simulation_params(simulation_params)
    res_orig_stochastic = sim_stochastic.get_pt()

    # stochastic simulation
    system_f = system(protocol_f)
    sim_stoch_f = TD_simulation_stochastic(system_f)
    res_stochastic = sim_stoch_f.s_A_to_B_tau_t(
        res_orig_stochastic["p"], res_orig_stochastic["t"], 0, 5, samplesize=samples, progress_bar_function=progress_bar_function
        )

    return res_stochastic


def calc_p_of_t(WTDs, Pis, N_event, params):
    # calculate distribution p(t)

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    # calculate p(t)
    print_debug("calculate p(t)", mode)
    p_of_t = np.zeros(N_t)
    for j in progress_bar_function(range(N_t)):
        # loop over t (0 -> T)

        for i in range(N_t - j):
            # integral over t0 (0 -> T-t)

            p_of_t_inc = dt / N_event["F1234-U"] * Pis["F1234"][i] * WTDs["F1234-U"][i, i + j]
            if np.isnan(p_of_t_inc) == False:
                p_of_t[j] += p_of_t_inc
    return p_of_t


def calc_p_of_tau(WTDs, Pis, N_event, params):
    # calculate distribution p(tau)

    # parameter
    N_t = params["N_t_WTD"]
    t = np.linspace(0, params["T"], N_t)
    dt = t[1] - t[0]
    progress_bar_function = params["progress_bar_function"]
    mode = params["mode"]

    # calculate p(tau)
    print_debug("calculate p(tau)", mode)
    p_of_tau = np.zeros(N_t)
    for i in progress_bar_function(range(N_t)):
        # loop over t0 (0 -> T)

        for j in range(N_t - i):
            # integral over t (0 -> T-t0)

            p_of_tau_inc = dt / N_event["F1234-U"] * Pis["F1234"][i] * WTDs["F1234-U"][i, i + j]
            if np.isnan(p_of_tau_inc) == False:
                p_of_tau[i] += p_of_tau_inc
    return p_of_tau
