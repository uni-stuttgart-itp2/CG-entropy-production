import tqdm
from multiprocessing import Pool
import pickle as pick
from systems.calmodulin import calmodulin 
from protocols.force_ramp import force_ramp
from aux import no_progressbar
import utils
import numpy as np


np.set_printoptions(edgeitems=30, linewidth=100000, formatter=dict(float=lambda x: "%.3g" % x))


def multiprocessing_s_of_tau(args):
    system, protocol_f, params = args
    result = utils.calc_s_of_tau(system, protocol_f, params)
    return result


# control parameter
save = False
multiprocessing = True
progress_bar = True

# simulation effort
N_t_WTD = int(1e7)
N_t_stoch = int(1e7)
N_t_hist = int(1e7)
sample_base = int(1e7)                                                                            # 40000000
N_threads = 100

# parameter
T = 1.5
f0 = 7
f1 = 12
slope = (f1-f0) / T

params = []
if multiprocessing:
    sample_base = int(sample_base/N_threads)
for i in range(N_threads):
    if ((not multiprocessing) or i == 0) and progress_bar:
        params.append({"T": T,
                       "f0": f0,
                       "slope": slope,
                       "sample_base": sample_base,
                       "N_t_WTD": N_t_WTD,
                       "N_t_stoch": N_t_stoch,
                       "num": i,
                       "progress_bar_function": tqdm.tqdm,
                       "mode": "v"})
    else:
        params.append({"T": T,
                       "f0": f0,
                       "slope": slope,
                       "sample_base": sample_base,
                       "N_t_WTD": N_t_WTD,
                       "N_t_stoch": N_t_stoch,
                       "num": i,
                       "progress_bar_function": no_progressbar,
                       "mode": "silent"})

# system
system = calmodulin

# protocol
protocol_f = force_ramp(T, f0, slope, reverse=False)
protocol_r = force_ramp(T, f0, slope, reverse=True)

# main
WTDs, rWTDs, Pis = utils.calc_WTDs(system, protocol_f, protocol_r, params[0])
N_event = utils.calc_normalizations(WTDs, Pis, params[0])
S_of_tau = utils.calc_S_of_tau(WTDs, rWTDs, Pis, N_event, params[0])
if multiprocessing is True:
    args = []
    for i in range(N_threads):
        args.append([system, protocol_f, params[i]])
    results_temp = []
    process = Pool(processes=N_threads)
    for result in process.imap(multiprocessing_s_of_tau, args):
        results_temp.append(result)
    process.close()
    process.join()
    results = []
    for temp in results_temp:
        for res in temp:
            results.append(res)
else:
    results = utils.calc_s_of_t(system, protocol_f, params[0])
bin_width = T / N_t_hist
tau_bin_centers = np.linspace(bin_width / 2, T - bin_width/2, N_t_hist)
dtau = tau_bin_centers[1] - tau_bin_centers[0]
s_of_tau = np.zeros(N_t_hist)
for res in results:
    s_of_tau[np.argmin(abs(res[1] - tau_bin_centers))] += res[0]
s_of_tau /= len(results) * dtau

# saving
results_data = {"S_of_tau": S_of_tau,
                "tau_est": np.linspace(0, T, N_t_WTD),
                "s_of_tau": s_of_tau,
                "tau_real": tau_bin_centers}
data = {
    "results": results_data,
    "params": {
        "T": T,
        "f0": f0,
        "f1": f1,
        "N_t (WTDs)": N_t_WTD,
        "N_t (stoch)": N_t_stoch,
        "N_t (hist)": N_t_hist,
        "sample_base": sample_base
        },
    "info": "protocol: force_ramp; system: calmodulin"
    }

if save:
    f = open("PATH_TO_SAVE", "wb")
    pick.dump(data, f)
    f.close()
