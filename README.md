# CG-entropy-production

This source code has been used for the following paper: General theory
for localizing the where and when of entropy production meets
single-molecule experiments

Authors: Julius Degünther, Jann van der Meer, and Udo Seifert

## Description

The following version of python was used: 3.12.4

The purpose of this source code is to compute the microscopic and
coarse-grained entropy production in a model for a protein folding
experiment with calmodulin.