import numpy as np
import tqdm
import pickle as pick
from systems.calmodulin import calmodulin
from protocols.force_ramp import force_ramp
from aux import no_progressbar
import utils


np.set_printoptions(edgeitems=30, linewidth=100000, formatter=dict(float=lambda x: "%.3g" % x))


# control parameter
save = False
multiprocessing = True
progress_bar = True

# simulation effort
N_t_WTD = int(1e7)

# parameter
T = 1.5
f0 = 7
f1 = 12
slope = (f1-f0) / T

params = {"T": T,
          "f0": f0,
          "slope": slope,
          "N_t_WTD": N_t_WTD,
          "progress_bar_function": tqdm.tqdm,
          "mode": "v"}

# system
system = calmodulin

# protocol
protocol_f = force_ramp(T, f0, slope, reverse=False)
protocol_r = force_ramp(T, f0, slope, reverse=True)

# main
WTDs, rWTDs, Pis = utils.calc_WTDs(system, protocol_f, protocol_r, params)
N_event = utils.calc_normalizations(WTDs, Pis, params)
p_of_tau = utils.calc_p_of_tau(WTDs, Pis, N_event, params)

# saving
results_data = {"p_of_tau": p_of_tau,
                "tau": np.linspace(0, T, N_t_WTD)}
data = {
    "results": results_data,
    "params": {
        "T": T,
        "f0": f0,
        "f1": f1,
        "N_t (WTDs)": N_t_WTD,
        },
    "info": "protocol: force_ramp; system: calmodulin"
    }

if save:
    f = open("PATH_TO_SAVE", "wb")
    pick.dump(data, f)
    f.close()
