import numpy as np
import tqdm
from multiprocessing import Pool
import pickle as pick
from systems.calmodulin import calmodulin
from protocols.force_ramp import force_ramp
from aux import no_progressbar
import utils


np.set_printoptions(edgeitems=30, linewidth=100000, formatter=dict(float=lambda x: "%.3g" % x))


def main_one_parameter_set(params):
    # main function that has to be executed for each set of parameters

    # system
    system = calmodulin

    # protocol
    protocol_f = force_ramp(params["T"], params["f0"], params["slope"], reverse=False)
    protocol_r = force_ramp(params["T"], params["f0"], params["slope"], reverse=True)

    # CG quantities
    WTDs, rWTDs, Pis = utils.calc_WTDs(system, protocol_f, protocol_r, params)
    N_event = utils.calc_normalizations(WTDs, Pis, params)
    Delta_S = utils.calc_S(WTDs, rWTDs, Pis, N_event, params)

    # micro quantities
    Delta_s, N_event_stoch = utils.calc_s(system, protocol_f, params)

    # relative probability for each type of trajectory
    Share = {key: N_event[key]/sum(N_event.values()) for key in N_event.keys()}
    Share_stoch = {key: N_event_stoch[key]/sum(N_event_stoch.values()) for key in N_event_stoch.keys()}

    return {"T": params["T"],
            "Delta_S": Delta_S,
            "Delta_s": Delta_s,
            "Share": Share,
            "Share_stoch": Share_stoch,
            "N_event_stoch": N_event_stoch,
            "N_event": N_event}


# control parameter
save = False
multiprocessing = True
progress_bar = True

# simulation effort
N_t_WTD = int(1e7)
N_t_stoch = int(1e7)
sample_base = int(1e7)
N_threads = 100

# parameter                                                            
N_T = 100
T_min = 0.1
T_max = 10
f0 = 7
f1 = 12
T_s = np.linspace(T_min, T_max, N_T)

params = []
for i in range(N_T):
    T = T_s[i]
    slope = (f1-f0) / T
    if ((not multiprocessing) or i == 0) and progress_bar:
        params.append({"T": T,
                       "f0": f0,
                       "slope": slope,
                       "sample_base": sample_base,
                       "N_t_WTD": N_t_WTD,
                       "N_t_stoch": N_t_stoch,
                       "num": i,
                       "progress_bar_function": tqdm.tqdm,
                       "mode": "v"})
    else:
        params.append({"T": T,
                       "f0": f0,
                       "slope": slope,
                       "sample_base": sample_base,
                       "N_t_WTD": N_t_WTD,
                       "N_t_stoch": N_t_stoch,
                       "num": i,
                       "progress_bar_function": no_progressbar,
                       "mode": "silent"})
        
results = {}
if multiprocessing:
    process = Pool(processes=N_threads)
    for result in tqdm.tqdm(process.imap(main_one_parameter_set, params), total=len(params)):
        results[result["T"]] = {"Delta_S": result["Delta_S"],
                                "Delta_s": result["Delta_s"],
                                "Share": result["Share"],
                                "Share_stoch": result["Share_stoch"],
                                "N_event_stoch": result["N_event_stoch"],
                                "N_event": result["N_event"]}
    process.close()
    process.join()
else:
    for param in params:
        result = main_one_parameter_set(param)
        results[result["T"]] = {"Delta_S": result["Delta_S"],
                                "Delta_s": result["Delta_s"],
                                "Share": result["Share"],
                                "Share_stoch": result["Share_stoch"],
                                "N_event_stoch": result["N_event_stoch"],
                                "N_event": result["N_event"]}

for T in results.keys():
    results[T]["Delta_s"]["F1234-rF1234"] = results[T]["Delta_s"].pop("0-0")
    results[T]["Delta_s"]["F1234-U"] = results[T]["Delta_s"].pop("0-5")
    results[T]["Delta_s"]["rU-rF1234"] = results[T]["Delta_s"].pop("5-0")
    results[T]["Delta_s"]["rU-U"] = results[T]["Delta_s"].pop("5-5")
    results[T]["Share_stoch"]["F1234-rF1234"] = results[T]["Share_stoch"].pop("0-0")
    results[T]["Share_stoch"]["F1234-U"] = results[T]["Share_stoch"].pop("0-5")
    results[T]["Share_stoch"]["rU-rF1234"] = results[T]["Share_stoch"].pop("5-0")
    results[T]["Share_stoch"]["rU-U"] = results[T]["Share_stoch"].pop("5-5")
    results[T]["N_event_stoch"]["F1234-rF1234"] = results[T]["N_event_stoch"].pop("0-0")
    results[T]["N_event_stoch"]["F1234-U"] = results[T]["N_event_stoch"].pop("0-5")
    results[T]["N_event_stoch"]["rU-rF1234"] = results[T]["N_event_stoch"].pop("5-0")
    results[T]["N_event_stoch"]["rU-U"] = results[T]["N_event_stoch"].pop("5-5")

data = {
    "results": results,
    "params": {
        "T_min": T_min,
        "T_max": T_max,
        "f0": f0,
        "f1": f1,
        "N_T": N_T,
        "N_t (WTDs)": N_t_WTD,
        "N_t (stoch)": N_t_stoch,
        "sample_base": sample_base
        },
    "info": "protocol: force_ramp; system: calmodulin"
    }

if save:
    f = open("PATH_TO_SAVE", "wb")
    pick.dump(data, f)
    f.close()
