import numpy as np


def D(f, x):
    df = np.zeros(len(f))
    df[:-1] = (f[1:] - f[:-1]) / (x[1:] - x[:-1])
    df[-1] = df[-2]
    return df


def antiderivative(f, x):
    F = [0]
    for i in range(1, len(x)):
        F.append(F[i - 1] + f[i] * (x[i] - x[i - 1]))
    return np.array(F)


def integrate(f, dx):
    F = sum(f) * dx
    return F


def print_matrix(m, prec=3):
    print("------------------------------------------")
    for i in range(len(m)):
        s = ""
        for j in range(len(m[0])):
            if m[i][j] == 0:
                s += "{:10.2f}".format(0)
            else:
                s += "{:10.2f}".format(m[i][j])
            s += " "
        print(s)
    print("------------------------------------------")


def print_debug(to_print, mode):
    if mode == "silent":
        pass
    else:
        print(to_print)


def no_progressbar(iterable):
    return iterable
