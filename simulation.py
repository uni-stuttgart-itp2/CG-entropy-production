import numpy as np
import scipy
import random
from aux import antiderivative
from aux import no_progressbar


class TD_simulation_stochastic:
    def __init__(self, system):
        self.system = system

    def get_integrated_rates(self, ts):
        N = self.system.N
        k = np.zeros((N, N, len(ts)))
        for l in range(len(ts)):
            k[:, :, l] = self.system.get_K_orig(ts[l])
        K = np.zeros((N, N, len(ts)))
        for i in range(N):
            for j in range(N):
                K[i, j, :] = antiderivative(k[i, j, :], ts)
        return K

    def s_A_to_B(self, p, ts, state_A, state_B, samplesize=10000, progress_bar_function=no_progressbar):
        # entropy production between A and B

        # integrated rates
        K = self.get_integrated_rates(ts)

        # setup
        N = self.system.N
        Ss = []

        # simulation
        for j in progress_bar_function(range(samplesize)):
            s = random.choices(np.arange(N), p[:, 0])[0]
            t_index = 0
            if s == state_A:
                in_snip = True
                S = 0
            else:
                in_snip = False
            while t_index < len(ts) - 1:
                r = [random.random() for i in range(N)]
                tau_index = np.ones(N, dtype=int) * int(1000 * len(ts))
                for i in range(N):
                    if i != s and K[s, i, -1] > 0:
                        tau_index[i] = np.argmin(abs(K[s, i, t_index:] - K[s, i, t_index] - np.log(1 / r[i])))
                if min(t_index + tau_index >= len(ts) - 1):
                    break
                t_index += int(min(tau_index))
                s_new = np.argmin(tau_index)
                if in_snip:
                    S += np.log(self.system.get_K_orig(ts[t_index])[s, s_new] / self.system.get_K_orig(ts[t_index])[s_new, s])
                    if s_new == state_B:
                        S -= np.log(p[state_B, t_index])
                        Ss.append(S)
                        S = 0
                        in_snip = False
                    elif s_new == state_A:
                        S = 0
                    elif s == state_A:
                        S += np.log(p[state_A, t_index])
                elif s_new == state_A:
                    in_snip = True
                    S = 0
                s = s_new
        return Ss

    def s_AB_all_combinations(self, p, ts, state_A, state_B, samplesize=10000, progress_bar_function=no_progressbar):
        # entropy production for paths between A and B: A->A, A->B, B->A, B->B

        # integrated rates
        K = self.get_integrated_rates(ts)

        # setup
        N = self.system.N
        Delta_s = {"%d-%d" % (state_A, state_B): [],
                   "%d-%d" % (state_A, state_A): [],
                   "%d-%d" % (state_B, state_B): [],
                   "%d-%d" % (state_B, state_A): []}
        N_event = {"%d-%d" % (state_A, state_B): 0,
                   "%d-%d" % (state_A, state_A): 0,
                   "%d-%d" % (state_B, state_B): 0,
                   "%d-%d" % (state_B, state_A): 0}
        N_total = 0

        # simulation
        snip = None
        for j in progress_bar_function(range(samplesize)):
            s = random.choices(np.arange(N), p[:, 0])[0]
            # s0s[s] += 1
            t_index = 0
            if s == state_A or s == state_B:
                snip = "%d-" % s
                # s_snip = np.log(p[s, t_index])
                s_snip = 0
            else:
                snip = None
                s_snip = 0
            while t_index < len(ts) - 1:
                r = [random.random() for i in range(N)]
                tau_index = np.ones(N, dtype=int) * int(1000 * len(ts))                            #np.inf
                for i in range(N):
                    if i != s and K[s, i, -1] > 0:
                        tau_index[i] = np.argmin(abs(K[s, i, t_index:] - K[s, i, t_index] - np.log(1 / r[i])))
                if min(t_index + tau_index >= len(ts) - 1):
                    break
                N_total += 1
                t_index += int(min(tau_index))
                s_new = np.argmin(tau_index)
                s_snip += np.log(self.system.get_K_orig(ts[t_index])[s, s_new] / self.system.get_K_orig(ts[t_index])[s_new, s])
                if s_new == state_A or s_new == state_B:
                    if snip is not None:
                        snip += "%d" % s_new
                        s_snip -= np.log(p[s_new, t_index])
                        Delta_s[snip].append(s_snip)
                        N_event[snip] += 1
                        snip = None
                    s_snip = 0
                elif s == state_A or s == state_B:
                    snip = "%d-" % s
                    s_snip += np.log(p[s, t_index])
                s = s_new
        for key in N_event.keys():
            N_event[key] /= samplesize
        return Delta_s, N_event

    def s_A_to_B_t(self, p, ts, state_A, state_B, samplesize=10000, progress_bar_function=no_progressbar):
        # entropy production for paths from A to B, A->B, with information about the duration t

        # integrated rates
        K = self.get_integrated_rates(ts)

        # setup
        N = self.system.N
        Ss = []

        # simulation
        for j in progress_bar_function(range(samplesize)):
            s = random.choices(np.arange(N), p[:, 0])[0]
            t_index = 0
            if s == state_A:
                in_snip = True
                S = 0
                t0 = ts[t_index]
            else:
                in_snip = False
            while t_index < len(ts) - 1:
                r = [random.random() for i in range(N)]
                tau_index = np.ones(N, dtype=int) * int(1000 * len(ts))
                for i in range(N):
                    if i != s and K[s, i, -1] > 0:
                        tau_index[i] = np.argmin(abs(K[s, i, t_index:] - K[s, i, t_index] - np.log(1 / r[i])))
                if min(t_index + tau_index >= len(ts) - 1):
                    break
                t_index += int(min(tau_index))
                s_new = np.argmin(tau_index)
                if in_snip:
                    S += np.log(self.system.get_K_orig(ts[t_index])[s, s_new] / self.system.get_K_orig(ts[t_index])[s_new, s])
                    if s_new == state_B:
                        S -= np.log(p[state_B, t_index])
                        Ss.append([S, ts[t_index] - t0])
                        S = 0
                        in_snip = False
                    elif s_new == state_A:
                        S = 0
                    elif s == state_A:
                        t0 = ts[t_index]
                        S += np.log(p[state_A, t_index])
                elif s_new == state_A:
                    in_snip = True
                    S = 0
                    t0 = ts[t_index]
                s = s_new
        return Ss

    def s_A_to_B_tau_t(self, p, ts, state_A, state_B, samplesize=10000, progress_bar_function=no_progressbar):
        # entropy production for paths from A to B, A->B, with information about initial time

        # integrated rates
        K = self.get_integrated_rates(ts)

        # setup
        N = self.system.N
        Ss = []

        # simulation
        for j in progress_bar_function(range(samplesize)):
            s = random.choices(np.arange(N), p[:, 0])[0]
            t_index = 0
            if s == state_A:
                in_snip = True
                S = 0
                t0 = ts[t_index]
            else:
                in_snip = False
            while t_index < len(ts) - 1:
                r = [random.random() for i in range(N)]
                tau_index = np.ones(N, dtype=int) * int(1000 * len(ts))
                for i in range(N):
                    if i != s and K[s, i, -1] > 0:
                        tau_index[i] = np.argmin(abs(K[s, i, t_index:] - K[s, i, t_index] - np.log(1 / r[i])))
                if min(t_index + tau_index >= len(ts) - 1):
                    break
                t_index += int(min(tau_index))
                s_new = np.argmin(tau_index)
                if in_snip:
                    S += np.log(self.system.get_K_orig(ts[t_index])[s, s_new] / self.system.get_K_orig(ts[t_index])[s_new, s])
                    if s_new == state_B:
                        S -= np.log(p[state_B, t_index])
                        Ss.append([S, t0, ts[t_index]])
                        S = 0
                        in_snip = False
                    elif s_new == state_A:
                        S = 0
                    elif s == state_A:
                        t0 = ts[t_index]
                        S += np.log(p[state_A, t_index])
                elif s_new == state_A:
                    in_snip = True
                    S = 0
                    t0 = ts[t_index]
                s = s_new
        return Ss


class TD_simulation_deterministic:
    def __init__(self, system, numeric_params):
        self.system = system
        self.set_numeric_params(numeric_params)

    def set_numeric_params(self, numeric_params):
        self.numeric_params = numeric_params
        self.t_total = np.linspace(self.numeric_params["t_min"], self.numeric_params["t_max"], self.numeric_params["N_t"])

    def set_simulation_params(self, simulation_params):
        self.simulation_params = simulation_params

    def dpdt(self, p, t):
        return np.dot(self.system.get_L(t), p)

    def sigma_tot(self, p, t):
        sigma = np.zeros(len(t))
        for k in range(len(t)):
            for i in range(self.system.N):
                for j in range(self.system.N):
                    if i != j:
                        sigma[k] += p[i][k] * self.system.get_K(t[k])[i][j] * np.log((p[i][k] * self.system.get_K(t[k])[i][j]) /
                                                                                     (p[j][k] * self.system.get_K(t[k])[j][i]))
        sigma_tot = 1 / (t[-1] - t[0]) * sum(sigma) * (t[1] - t[0])
        return sigma_tot

    def s_tot(self, p, t):
        s = np.zeros(len(t))
        for k in range(len(t)):
            for i in range(self.system.N):
                for j in range(self.system.N):
                    if i != j:
                        s[k] += p[i][k] * self.system.get_K(t[k])[i][j] * np.log((p[i][k] * self.system.get_K(t[k])[i][j]) /
                                                                                 (p[j][k] * self.system.get_K(t[k])[j][i]))
        s_tot = sum(s) * (t[1] - t[0])
        return s_tot

    def get_pt(self):
        N_t = np.argmin(abs(self.t_total - self.simulation_params["t_end"])) - np.argmin(abs(self.t_total - self.simulation_params["t_start"])) + 1
        t = np.linspace(self.simulation_params["t_start"], self.simulation_params["t_end"], N_t)
        p_t = scipy.integrate.odeint(self.dpdt, self.simulation_params["p0"], t)
        return {"t": t, "p": np.transpose(p_t)}

    def get_pt_sigmatot(self):
        t = np.linspace(self.simulation_params["t_start"], self.simulation_params["t_end"], self.numeric_params["N_t"])
        p_t = np.transpose(scipy.integrate.odeint(self.dpdt, self.simulation_params["p0"], t))
        sigma = self.sigma(p_t, t)
        return {"t": t, "p": p_t, "sigma_tot": sigma}

    def get_pt_stot(self):
        t = np.linspace(self.simulation_params["t_start"], self.simulation_params["t_end"], self.numeric_params["N_t"])
        p_t = np.transpose(scipy.integrate.odeint(self.dpdt, self.simulation_params["p0"], t))
        s_tot = self.s_tot(p_t, t)
        return {"t": t, "p": p_t, "s_tot": s_tot}
